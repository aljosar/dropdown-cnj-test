# dropdown-cnj-test

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Dropdown component usage

Simply register component in your vue file:

```
import Dropdown from './components/Dropdown.vue';

...

components: {
    Dropdown
  }
```

And then use it in your template:

```
<Dropdown :content="dropdownContent"/>
```

where dropdownContent is an array of objects with value and id properties.
Example: ```[{ value: 'First item', id: 0 }, { value: 'Second item', id: 1 }, { value: 'Third item', id: 2 }, { value: 'Fourth item', 	id: 3 }, { value: 'Fifth item', id: 4 }]```

